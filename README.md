# backend

This project contains the backend (and the static front) for the InterHack2019 competition team 6 at IME-USP.

The project runs over koa.js + firestore for db.

The service is hosted at http://birds.digital:4001 or http://34.66.73.111:4001.

The project USProcedimentos is a project tracker intended to help people understand the progress of projects.
