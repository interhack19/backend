// USProcedimentos

// Firebase Stuff
const admin = require('firebase-admin')
let serviceAccount = require('./key.json')

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
})
  
let db = admin.firestore()


// Koa Stuff
const Koa = require('koa')
const app = new Koa()
const koaBody = require('koa-body') // Add support for resquest.body
app.use(koaBody());
const koaStatic = require('koa-static') // Add support for static
app.use(koaStatic("static"))

// Controller
const getProcedures = async () => {
    var ret = []
    var snapshot = await db.collection('procedureList').get()
    snapshot.forEach((doc) => {ret.push(doc.data())}) 

    return ret
}

const getDetailedProcedure = async (id) => {
    console.log(id)
    var doc = await db.collection('procedures').doc(id).get()
    return doc.exists ? doc.data() : "no document"
}

const simpleOf = (post) => {
    var ret = {}
    ret.title = post.title

    let total = 0
    let done = 0
    post.progress.forEach((x) => { total += 1; done += (x.state == "completed") ? 1 : 0 })

    ret.progress = {done: done, total: total}
    ret.state = post.state
    ret.tag = post.tag
    ret.id = post.id

    console.log(ret)
    return ret
}

const createDetailedProcedure = async (post) => {
    let doc = db.collection("controller").doc("state")
    let state = await doc.get()
    if(state.exists) {
        let tmp = state.data()
        let newId = tmp.nextValid
        post.id = newId
        tmp.nextValid = (1 + parseInt(tmp.nextValid)).toString()
        doc.set(tmp)
        db.collection('procedures').doc(post.id).set(post)
        db.collection('procedureList').doc(post.id).set(simpleOf(post))
        return newId
    }
}

const updateDetailedProcedure = async (post) => {
    db.collection("procedures").doc(post.id).set(post)
    db.collection("procedureList").doc(post.id).set(simpleOf(post))
    return "Saved"
}

// Application
app.use(async ctx => {
    console.log(ctx.method)
    console.log(ctx.path)
    if(ctx.method == "GET" && ctx.path == "/procedures") {
        ctx.response.type = "application/json"
        ret = await getProcedures()
        ctx.body = JSON.stringify(ret)
    } else if(ctx.path == "/detailedProcedure") {
        if(ctx.method == "GET") {
            ctx.response.type = "application/json"
            ret = await getDetailedProcedure(ctx.query.id)
            ctx.body = JSON.stringify(ret)
        } else if(ctx.method == "PUT") {
            ctx.response.type = "application/json"
            console.log(ctx.request.body)
            ret = await createDetailedProcedure(ctx.request.body)
            ctx.body = JSON.stringify(ret)
        } else if(ctx.method == "POST") {
            ctx.response.type = "application/json"
            console.log(ctx.request.body)
            ret = await updateDetailedProcedure(ctx.request.body)
            ctx.body = JSON.stringify(ret)
        }
    } else {
        ctx.status = 404
    }
});

app.listen(4001);